﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class HolidayDate
    {
        [Key]
        public int HolidayDateId { get; set; }

        [Display(Name = "Tanggal Mulai")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Tanggal Selesai")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Hari libur wajib diisi")]
        [Display(Name = "Nama Hari Libur")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Tipe wajib diisi")]
        [Display(Name = "Tipe")]
        public string Type { get; set; }

        public double Total
        {
            get
            {
                double total = (EndDate - StartDate).TotalDays;
                return total;
            }
        }
    }
}
