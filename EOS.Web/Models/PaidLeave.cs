﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;
using EOS.Web.CustomValidation;
using Microsoft.AspNetCore.Mvc;

namespace EOS.Web.Models
{
    public class PaidLeave
    {
        [Key]
        public int PaidLeaveId { get; set; }

        [Display(Name = "Tanggal Mulai")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Tanggal Selesai")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Tipe Cuti wajib diisi")]
        [Display(Name = "Tipe")]
        public int TypeId { get; set; }

        [Required(ErrorMessage = "Periode Cuti wajib diisi")]
        [Display(Name = "Periode")]
        public int PeriodId { get; set; }

        [Required(ErrorMessage = "Alasan Cuti wajib diisi")]
        [Display(Name = "Keterangan")]
        //[CustomPaidLeave]
        public string Remarks { get; set; }

        public AppUser Requestor { get; set; }

        public string RequestorId { get; set; }

        [Display(Name = "Tanggal Request")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime RequestDate { get; set; }

        [Display(Name = "Total Hari")]
        [Remote("VerifyLimitPeriod","PaidLeaves", AdditionalFields = nameof(PeriodId))]
        public double Value { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Tipe")]
        public virtual Type Type { get; set; }

        [Display(Name = "Periode")]
        public virtual PaidLeavePeriod Period { get; set; }

        public string FirstApprover { get; set; }

        public DateTime FirstApproverTimestamp { get; set; }

        public string FirstApproverResponse { get; set; }

        public string SecondApprover { get; set; }

        public DateTime SecondApproverTimestamp { get; set; }

        public string SecondApproverResponse { get; set; }

        public string SubmissionType { get; set; }

        [Display(Name = "Upload Foto")]
        public byte[] PaidLeavePhoto { get; set; }

        //[Display(Name = "Dari Jam")]
        //public virtual string From { get; set; }

        //[Display(Name = "Sampai Jam")]
        //public virtual string To { get; set; }
    }
}
