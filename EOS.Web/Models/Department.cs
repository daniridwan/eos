﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class Department
    {
        [Key]
        public int DepartmentId { get; set; }
        [Required(ErrorMessage = "Nama Department wajib diisi")]
        [Display(Name = "Nama Department")]
        public string DepartmentName { get; set; }
        //section head
        public string DepartmentHead { get; set; }
    }
}
