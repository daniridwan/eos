﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class Division
    {
        [Key]
        public int DivisionId { get; set; }
        [Required(ErrorMessage = "Nama Divisi wajib diisi")]
        [Display(Name = "Nama Divisi")]
        public string DivisionName { get; set; }
    }
}
