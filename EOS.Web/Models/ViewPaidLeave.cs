﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class ViewPaidLeave
    {
        //class constructor
        public ViewPaidLeave(int paidLeaveId, DateTime startDate, DateTime endDate, int typeId, string paidLeaveName, string remarks, string status, string requestorId, DateTime requestDate, double value, string firstApprover, string firstApproverResponse, DateTime firstApproverTimestamp, string secondApprover, string secondApproverResponse, DateTime secondApproverTimestamp, string fullName)
        {
            this.PaidLeaveId = paidLeaveId;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.TypeId = typeId;
            this.PaidLeaveName = paidLeaveName;
            this.Remarks = remarks;
            this.Status = status;
            this.RequestorId = requestorId;
            this.RequestDate = requestDate;
            this.Value = value;
            this.FirstApprover = firstApprover;
            this.FirstApproverResponse = firstApproverResponse;
            this.FirstApproverTimestamp = firstApproverTimestamp;
            this.SecondApprover = secondApprover;
            this.SecondApproverResponse = secondApproverResponse;
            this.SecondApproverTimestamp = secondApproverTimestamp;
            this.Fullname = fullName; //requestor name
        }

        public ViewPaidLeave()
        { }

        //class attributes
        public int PaidLeaveId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TypeId { get; set; }
        public string PaidLeaveName { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }
        public string RequestorId { get; set; }
        public DateTime RequestDate { get; set; }
        public double Value { get; set; }
        public string FirstApprover { get; set; }
        public string FirstApproverResponse { get; set; }
        public DateTime FirstApproverTimestamp { get; set; }
        public string SecondApprover { get; set; }
        public string SecondApproverResponse { get; set; }
        public DateTime SecondApproverTimestamp { get; set; }
        public string Fullname { get; set; }
        //helper
        public string Response { get; set; }
    }
}
