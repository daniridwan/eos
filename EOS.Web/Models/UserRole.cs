﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EOS.Web.Models
{
    public class UserRole
    {
        public AppUser ApplicationUser { get; set; }
        public List<SelectListItem> ApplicationRoles { get; set; }
    }
}
