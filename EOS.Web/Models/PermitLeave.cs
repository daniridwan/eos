﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class PermitLeave
    {
        [Key]
        public int PermitLeaveId { get; set; }

        [Display(Name = "Tanggal Mulai")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "Tanggal Selesai")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime EndDate { get; set; }

        [Required(ErrorMessage = "Periode Cuti wajib diisi")]
        [Display(Name = "Periode")]
        public int PeriodId { get; set; }

        [Display(Name = "Periode")]
        public virtual PaidLeavePeriod Period { get; set; }
    }
}
