﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class Type
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Nama bagian wajib diisi")]
        [Display(Name = "Tipe Cuti")]
        public string PaidLeaveName { get; set; }

        [Required(ErrorMessage = "Jumlah hari cuti wajib diisi")]
        [Display(Name = "Jumlah")]
        public int Value { get; set; }

        public string NameValue {
            get{
                return string.Format("{0} ({1})", PaidLeaveName, Value);
            }
        }
    }
}
