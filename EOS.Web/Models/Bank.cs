﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class Bank
    {
        [Key]
        public int BankId { get; set; }

        [Required(ErrorMessage = "Nama Bank wajib diisi")]
        [Display(Name = "Nama Bank")]
        public string BankName { get; set; }
    }
}
