﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class ViewRemainingLeave
    {
        //class attributes
        public int Id { get; set; }
        public string PeriodName { get; set; }
        public double LeaveAllowance { get; set; }
        public double RemainingLeave { get; set; }
        public double ActualLeave { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
