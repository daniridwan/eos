﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class ViewPaidLeaveDetail
    {
        //class attributes
        [Key]
        public int PaidLeaveId { get; set; }
        public string RequestorId { get; set; }
        public string FullName { get; set; }
        public string PaidLeaveName { get; set; }
        public string SubmissionType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public int PeriodId { get; set; }
        public double Value { get; set; }
        public string PeriodName { get; set; }
        public int TypeId { get; set; }
    }
}
