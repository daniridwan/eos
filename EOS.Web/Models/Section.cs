﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class Section
    {
        [Key]
        public int SectionId { get; set; }
        [Required(ErrorMessage = "Nama Bagian wajib diisi")]
        [Display(Name = "Nama Bagian")]
        public string SectionName { get; set; }
        //section head
        public string SectionHead { get; set; }
    }
}
