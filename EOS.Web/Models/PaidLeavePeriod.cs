﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using EOS.Web.Areas.Identity.Data;

namespace EOS.Web.Models
{
    public class PaidLeavePeriod
    {
        //private AppUser _user;

        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Periode wajib diisi")]
        [Display(Name = "Periode")]
        public string PeriodName { get; set; }

        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndDate { get; set; }

        public double Value { get; set; }

        public int Status { get; set; }

        public AppUser User { get; set; }

        public string UserId { get; set; }

        //helper
        public string StatusString
        {
            get
            {
                if (Status == 1)
                {
                    return "Aktif";
                }
                else
                {
                    return "Inaktif";
                }
            }
        }
    }
}
