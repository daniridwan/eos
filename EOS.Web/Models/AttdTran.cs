﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EOS.Web.Models
{
    public class AttdTran
    {
        [Key]
        public int AttdTranId { get; set; }

        public int AttdState { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy hh:mm:ss}")]
        public DateTime ClockingHour { get; set; }

        public int EnrollNumber { get; set; }

        public bool Invalid { get; set; }

        public string Source { get; set; }

        public int VerifyMethod { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy hh:mm:ss}")]
        public DateTime DateIn { get; set; }

        public string ProgIn { get; set; }

        public int UserIn { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MMM-yyyy hh:mm:ss}")]
        public DateTime DateUp { get; set; }

        public string ProgUp { get; set; }

        public int UserUp { get; set; }
        
        public string StsRec { get; set; }

        public string StateType
        {
            get
            {
                if (AttdState == 0 )
                {
                    return "Check In";
                }
                else if (AttdState == 1)
                {
                    return "Check Out";
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
