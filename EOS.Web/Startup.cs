using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EOS.Web.Areas.Identity.Data;
using EOS.Web.Data;
using EOS.Web.Entities;
using EOS.Web.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using NETCore.MailKit.Extensions;
using NETCore.MailKit.Infrastructure.Internal;
using Microsoft.Extensions.Logging;

namespace EOS.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EOSContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("EOSContextConnection")));

            services.AddIdentity<AppUser, IdentityRole>(options =>
            {
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireDigit = false;
                options.SignIn.RequireConfirmedAccount = false;
            }).AddEntityFrameworkStores<EOSContext>()
            .AddDefaultUI()
            .AddRoleManager<RoleManager<IdentityRole>>()
            .AddDefaultTokenProviders();

                //services.AddDefaultIdentity<AppUser>(options => {
                //    options.Password.RequireLowercase = false;
                //    options.Password.RequireUppercase = false;
                //    options.Password.RequireDigit = false;
                //    options.SignIn.RequireConfirmedAccount = false;
                //}).AddEntityFrameworkStores<EOSContext>();

            services.AddControllersWithViews();
            services.AddRazorPages(options => {
                options.Conventions.AllowAnonymousToPage("/Identity/Account/Login");
                options.Conventions.AllowAnonymousToPage("/Identity/Account/Logout");
            });
            services.ConfigureApplicationCookie(options =>
            {
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                options.LoginPath = "/Identity/Account/Login";
                options.LogoutPath = "/Identity/Account/Logout";
                options.AccessDeniedPath = "/Identity/Account/Login";
                //option.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                //options.Cookie.Name = "RPUApps";
            });

            //services.AddMvc().AddViewOptions(options =>
            //{
            //    options.HtmlHelperOptions.ClientValidationEnabled = true;
            //});

            //email
            services.Configure<SmtpSettings>(Configuration.GetSection("SmtpSettings"));
            services.AddSingleton<IMailer, Mailer>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            var path = Directory.GetCurrentDirectory();
            loggerFactory.AddFile($"{path}\\Logs\\Log.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
