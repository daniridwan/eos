﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Models;

namespace EOS.Web.Data
{
    public class EOSContext : IdentityDbContext<AppUser>
    {
        public EOSContext(DbContextOptions<EOSContext> options)
            : base(options)
        {
        }

        public DbSet<AppUser> AppUsers { get; set; }

        public DbSet<Bank> Bank { get; set; }

        public DbSet<EOS.Web.Models.Section> Section { get; set; }

        public DbSet<EOS.Web.Models.Division> Division { get; set; }

        public DbSet<EOS.Web.Models.Department> Department { get; set; }

        public DbSet<EOS.Web.Models.Grade> Grade { get; set; }

        public DbSet<EOS.Web.Models.Job> Job { get; set; }

        public DbSet<EOS.Web.Models.HolidayDate> HolidayDate { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<ViewRemainingLeave>(vrl =>
            {
                vrl.HasNoKey();
                vrl.ToView("View_Remaining_Leave");
            });
            builder.Entity<ViewAgendaList>(val =>
            {
                val.HasNoKey();
                val.ToView("View_Agenda_List");
            });
        }        

        public DbSet<EOS.Web.Models.PaidLeave> PaidLeave { get; set; }

        public DbSet<EOS.Web.Models.Type> Type { get; set; }

        public DbSet<EOS.Web.Models.PaidLeavePeriod> PaidLeavePeriod { get; set; }

        public DbSet<EOS.Web.Models.ViewRemainingLeave> ViewRemainingLeaves { get; set; }

        public DbSet<EOS.Web.Models.ViewAgendaList> ViewAgendaLists { get; set; }

        public DbSet<EOS.Web.Models.ViewPaidLeaveDetail> ViewPaidLeaveDetails { get; set; }

        public DbSet<EOS.Web.Models.AttdTran> AttdTran { get; set; }
    }
}
