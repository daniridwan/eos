﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using EOS.Web.Models;

namespace EOS.Web.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the AppUser class
    public class AppUser : IdentityUser
    {
        [PersonalData]
        [Column(TypeName = "varchar(100)")]
        [Required(ErrorMessage = "Nama lengkap wajib diisi")]
        [Display(Name = "Nama Lengkap")]
        public string FullName { get; set; }

        [PersonalData]
        [Column(TypeName = "varchar(50)")]
        [Display(Name = "Tempat Lahir")]
        public string BirthPlace { get; set; }

        [PersonalData]
        [Column(TypeName = "Date")]
        [Display(Name = "Tanggal Lahir")]
        [Required(ErrorMessage = "Tanggal Lahir wajib diisi")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime BirthDate { get; set; }

        [PersonalData]
        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "No. Karyawan wajib diisi")]
        [Display(Name = "NIK")]
        public string EmployeeNumber { get; set; }

        [PersonalData]
        [Column(TypeName = "Date")]
        [Display(Name = "Tgl. Masuk")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Tanggal Mulai Bekerja wajib diisi")]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime StartJoinDate { get; set; }

        [PersonalData]
        [Column(TypeName = "Date")]
        [Display(Name = "Tgl. Keluar")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime EndJoinDate { get; set; }

        [PersonalData]
        [Column(TypeName = "varchar(MAX)")]
        public string Address { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Seksi wajib dipilih")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Seksi wajib dipilih")]
        public int SectionId { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Departemen wajib dipilih")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Departemen wajib dipilih")]
        public int DepartmentId { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Divisi wajib dipilih")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Divisi wajib dipilih")]
        public int DivisionId { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Job Title wajib dipilih")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Job Title wajib dipilih")]
        public int JobId { get; set; }

        [PersonalData]
        [Required(ErrorMessage = "Grade wajib dipilih")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Grade wajib dipilih")]
        public int GradeId { get; set; }

        //[PersonalData]
        //public int BankId { get; set; }

        public virtual Section Section { get; set; }

        public virtual Department Department { get; set; }

        public virtual Division Division { get; set; }

        public virtual Job Job { get; set; }

        public virtual Grade Grade { get; set; }

        //public virtual Bank Bank { get; set; }

        [PersonalData]
        [Column(TypeName = "varchar(4)")]
        public string TaxStatus { get; set; }

        [PersonalData]
        [Display(Name = "Upload Foto")]
        public byte[] UserPhoto { get; set; }

        [PersonalData]
        [Column(TypeName = "varchar(5)")]
        [Required(ErrorMessage = "Tipe wajib dipilih")]
        public string EmployeeType { get; set; }

        [PersonalData]
        [Range(1, Int32.MaxValue, ErrorMessage = "Sync ID wajib dipilih")]
        public int SyncId { get; set; }
    }
}
