﻿using System;
using EOS.Web.Areas.Identity.Data;
using EOS.Web.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(EOS.Web.Areas.Identity.IdentityHostingStartup))]
namespace EOS.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            //builder.ConfigureServices((context, services) => {
            //    services.AddDbContext<EOSContext>(options =>
            //        options.UseSqlServer(
            //            context.Configuration.GetConnectionString("EOSContextConnection")));

            //    //services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //    //    .AddEntityFrameworkStores<EOSContext>();
            //    services.AddDefaultIdentity<AppUser>(options => {
            //        options.Password.RequireLowercase = false;
            //        options.Password.RequireUppercase = false;
            //        options.SignIn.RequireConfirmedAccount = false;
            //    })
            //        .AddEntityFrameworkStores<EOSContext>();
            //});
        }
    }
}