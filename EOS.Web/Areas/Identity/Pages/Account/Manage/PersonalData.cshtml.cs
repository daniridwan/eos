﻿using System.Threading.Tasks;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using EOS.Web.Data;

namespace EOS.Web.Areas.Identity.Pages.Account.Manage
{
    public class PersonalDataModel : PageModel
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<PersonalDataModel> _logger;
        private readonly EOSContext _context;

        public PersonalDataModel(UserManager<AppUser> userManager, ILogger<PersonalDataModel> logger, EOSContext context)
        {
            _userManager = userManager;
            _logger = logger;
            _context = context;
        }

        public string Username { get; set; }
        public string Fullname { get; set; }
        public string EmployeeNumber { get; set; }
        public string BirthPlace { get; set; }
        public string BirthDate { get; set; }
        public string Address { get; set; }
        public string JoinDate { get; set; }
        public string EndDate { get; set; }
        public string Section { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string TaxStatus { get; set; }

        private async Task LoadAsync(AppUser user)
        {
            Username = await _userManager.GetUserNameAsync(user);
            Fullname = user.FullName;
            EmployeeNumber = user.EmployeeNumber;
            BirthPlace = user.BirthPlace;
            BirthDate = user.BirthDate.ToString("dd MMMM yyyy");
            Address = user.Address;
            JoinDate = user.StartJoinDate.ToString("dd MMMM yyyy");
            if(user.EndJoinDate == DateTime.MinValue){
                EndDate = "n/a";
            }
            else{
                EndDate = user.EndJoinDate.ToString("dd MMMM yyyy");
            }
            var s = await _context.Section.FindAsync(user.SectionId);
            var d = await _context.Department.FindAsync(user.DepartmentId);
            var di = await _context.Division.FindAsync(user.DivisionId);
            Section = s.SectionName;
            Department = d.DepartmentName;
            Division = di.DivisionName;
            TaxStatus = user.TaxStatus;
        }

        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }
    }
}