﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace EOS.Web.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public IndexModel(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public string Username { get; set; }
        public string Fullname { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [DataType(DataType.Text)]
            [Display(Name = "Address")]
            public string Address { get; set; }

            [DataType(DataType.Text)]
            [Display(Name = "Status Pajak")]
            public string TaxStatus { get; set; }

            [Display(Name = "Foto Profil")]
            public byte[] UserPhoto { get; set; }

            [Display(Name = "Tempat Lahir")]
            public string BirthPlace { get; set; }

            [Display(Name = "Tanggal Lahir")]
            [Required(ErrorMessage = "Tanggal Lahir wajib diisi")]
            [DataType(DataType.Date)]
            public DateTime BirthDate { get; set; }
        }

        private async Task LoadAsync(AppUser user)
        {
            var userName = await _userManager.GetUserNameAsync(user);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            var address = user.Address;
            var taxStatus = user.TaxStatus;
            var userPhoto = user.UserPhoto;
            var birthPlace = user.BirthPlace;
            var birthDate = user.BirthDate;

            Username = userName;
            Fullname = user.FullName;

            Input = new InputModel
            {
                PhoneNumber = phoneNumber,
                Address = address,
                TaxStatus = taxStatus,
                UserPhoto = userPhoto,
                BirthPlace = birthPlace,
                BirthDate = birthDate
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(user);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(user);
                return Page();
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    StatusMessage = "Unexpected error when trying to set phone number.";
                    return RedirectToPage();
                }
            }

            if (Input.Address != user.Address) {
                user.Address = Input.Address;
            }

            if (Input.TaxStatus != user.TaxStatus)
            {
                user.TaxStatus = Input.TaxStatus;
            }

            if (Input.BirthPlace != user.BirthPlace)
            {
                user.BirthPlace = Input.BirthPlace;
            }

            if (Input.BirthDate != user.BirthDate)
            {
                user.BirthDate = Input.BirthDate;
            }

            if (Request.Form.Files.Count > 0)
            {
                IFormFile file = Request.Form.Files.FirstOrDefault();
                using (var dataStream = new MemoryStream())
                {
                    await file.CopyToAsync(dataStream);
                    user.UserPhoto = dataStream.ToArray();
                }
                await _userManager.UpdateAsync(user);
            }

            await _userManager.UpdateAsync(user);

            await _signInManager.RefreshSignInAsync(user);
            StatusMessage = "Profil anda sudah diupdate dan direview oleh HR";
            return RedirectToPage();
        }
    }
}
