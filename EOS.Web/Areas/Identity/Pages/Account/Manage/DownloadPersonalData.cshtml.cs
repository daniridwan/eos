﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.IO;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using EOS.Web.Data;

namespace EOS.Web.Areas.Identity.Pages.Account.Manage
{
    public class DownloadPersonalDataModel : PageModel
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<DownloadPersonalDataModel> _logger;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly EOSContext _context;
        private XSSFWorkbook xssfworkbook;
        private Dictionary<int, ICellStyle> cellStyleLookup;

        public DownloadPersonalDataModel(UserManager<AppUser> userManager, ILogger<DownloadPersonalDataModel> logger, IWebHostEnvironment env, EOSContext context)
        {
            _userManager = userManager;
            _logger = logger;
            _hostingEnvironment = env;
            _context = context;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            _logger.LogInformation("User with ID '{UserId}' asked for their personal data.", _userManager.GetUserId(User));

            // Only include personal data for download
            var personalData = new Dictionary<string, string>();
            var personalDataProps = typeof(AppUser).GetProperties().Where(
                            prop => Attribute.IsDefined(prop, typeof(PersonalDataAttribute)));
            foreach (var p in personalDataProps)
            {
                personalData.Add(p.Name, p.GetValue(user)?.ToString() ?? "null");
            }

            var logins = await _userManager.GetLoginsAsync(user);
            foreach (var l in logins)
            {
                personalData.Add($"{l.LoginProvider} external login provider key", l.ProviderKey);
            }

            //export to excel
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"PersonalData.xlsx";
            //string URL = string.Format("{0}://{1}/export/{2}", Request.Scheme, Request.Host, sFileName);
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, "export", sFileName));
            //var memory = new MemoryStream();
            //using (var fs = new FileStream(file.FullName, FileMode.Open, FileAccess.ReadWrite))
            //{
            //    IWorkbook workbook;
            //    workbook = new XSSFWorkbook(fs);
            //    ISheet excelSheet = workbook.GetSheetAt(0);
            //    IRow row = excelSheet.GetRow(7);

            //    row.GetCell(2).SetCellValue("AAAAAAA");

            //    ICell cell = workbook.GetSheetAt(0).GetRow(7).GetCell(2);
            //    string val = cell.ToString();

            //    workbook.Write(fs);
            //}

            //using (var stream = new FileStream(Path.Combine(sWebRootFolder, "export", sFileName), FileMode.Open))
            //{
            //    await stream.CopyToAsync(memory);
            //}
            //memory.Position = 0;
            //Response.Headers.Add("Content-Length", memory.Length.ToString());
            //Response.Headers.Add("Content-Disposition", "attachment; filename=PersonalData.xlsx");
            //return File(memory, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);

            byte[] result = RenderExcel(personalData, user);
            Response.Headers.Add("ContentType","application/vnd.ms-excel");
            Response.Headers.Add("Content-Length", result.Length.ToString());
            Response.Headers.Add("Content-Disposition", "attachment; filename=\"Company.xls\"");
            return File(result, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", sFileName);
        }

        public byte[] RenderExcel(Dictionary<string, string> personalData, AppUser user)
        {
            string sWebRootFolder = _hostingEnvironment.WebRootPath;
            string sFileName = @"PersonalData.xlsx";
            FileInfo file = new FileInfo(Path.Combine(sWebRootFolder, "export", sFileName));
            XSSFWorkbook hssfworkbook = LoadTemplate(file.FullName);
            ISheet sheet = hssfworkbook.GetSheet("PersonalData");

            cellStyleLookup = BuildRowStyleLookup(sheet.GetRow(7), 0, 20);

            int current_row = 7;
            for(int i = 0;i<=13;i++)
            {
                IRow row = sheet.GetRow(current_row + i);
                if (row == null)
                {
                    row = sheet.CreateRow(current_row + i);
                }
            }
            string val = "";
            if(personalData.TryGetValue("UserName", out val))
            {
                CreateCell(sheet.GetRow(current_row), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("FullName", out val))
            {
                CreateCell(sheet.GetRow(current_row + 1), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 1), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("EmployeeNumber", out val))
            {
                CreateCell(sheet.GetRow(current_row + 2), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 2), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("BirthPlace", out val))
            {
                CreateCell(sheet.GetRow(current_row + 3), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 3), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("BirthDate", out val))
            {
                DateTime d = Convert.ToDateTime(val);
                CreateCell(sheet.GetRow(current_row + 4), 1).SetCellValue(d.ToString("dd MMMM yyyy"));
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 4), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("Address", out val))
            {
                
                CreateCell(sheet.GetRow(current_row + 5), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 5), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("StartJoinDate", out val))
            {
                DateTime d = Convert.ToDateTime(val);
                CreateCell(sheet.GetRow(current_row + 6), 1).SetCellValue(d.ToString("dd MMMM yyyy"));
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 6), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("EndJoinDate", out val))
            {
                DateTime d = Convert.ToDateTime(val);
                if(val != "1/1/01 12:00:00 AM")
                {
                    CreateCell(sheet.GetRow(current_row + 7), 1).SetCellValue(d.ToString("dd MMMM yyyy"));
                }
                else
                {
                    CreateCell(sheet.GetRow(current_row + 7), 1).SetCellValue("");
                }
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 7), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("SectionId", out val))
            {
                var s = _context.Section.Find(Convert.ToInt32(val));
                CreateCell(sheet.GetRow(current_row + 8), 1).SetCellValue(s.SectionName);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 8), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("DepartmentId", out val))
            {
                var d = _context.Department.Find(Convert.ToInt32(val));
                CreateCell(sheet.GetRow(current_row + 9), 1).SetCellValue(d.DepartmentName);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 9), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("DivisionId", out val))
            {
                var d = _context.Division.Find(Convert.ToInt32(val));
                CreateCell(sheet.GetRow(current_row + 10), 1).SetCellValue(d.DivisionName);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 10), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("GradeId", out val))
            {
                var g = _context.Grade.Find(Convert.ToInt32(val));
                CreateCell(sheet.GetRow(current_row + 11), 1).SetCellValue(g.GradeName);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 11), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("JobId", out val))
            {
                var j = _context.Job.Find(Convert.ToInt32(val));
                CreateCell(sheet.GetRow(current_row + 12), 1).SetCellValue(j.JobTitle);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 12), 1).SetCellValue("");
            }
            if (personalData.TryGetValue("TaxStatus", out val))
            {
                CreateCell(sheet.GetRow(current_row + 13), 1).SetCellValue(val);
            }
            else
            {
                CreateCell(sheet.GetRow(current_row + 13), 1).SetCellValue("");
            }
            //picture
            if(personalData.TryGetValue("UserPhoto", out val))
            {
                byte[] bytes = user.UserPhoto;
                int pictureIndex = hssfworkbook.AddPicture(bytes, PictureType.PNG);
                ICreationHelper helper = hssfworkbook.GetCreationHelper();
                IDrawing drawing = sheet.CreateDrawingPatriarch();
                IClientAnchor anchor = helper.CreateClientAnchor();
                anchor.Col1 = 4;//0 index based column
                anchor.Row1 = 7;//0 index based row
                IPicture picture = drawing.CreatePicture(anchor, pictureIndex);
                picture.Resize(1.5f, 16.5f);
            }
            
            sheet.ForceFormulaRecalculation = true;
            MemoryStream ms = new MemoryStream();
            hssfworkbook.Write(ms);
            return ms.ToArray();
        }

        private XSSFWorkbook LoadTemplate(string path)
        {
            FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read);
            xssfworkbook = new XSSFWorkbook(file);
            return xssfworkbook;
        }

        private ICell CreateCell(IRow row, int cellIndex)
        {
            ICell cell = row.GetCell(cellIndex);
            if (cell == null)
            {
                cell = row.CreateCell(cellIndex);
                cell.CellStyle.WrapText = true;
            }
            if (cellStyleLookup.ContainsKey(cellIndex))
            {
                ICellStyle originalstyle = cellStyleLookup[cellIndex];
                cell.CellStyle = originalstyle;
                cell.CellStyle.WrapText = true;
            }
            return cell;
        }

        private Dictionary<int, ICellStyle> BuildRowStyleLookup(IRow row, int startCellIndex, int count)
        {
            Dictionary<int, ICellStyle> colStyleMap = new Dictionary<int, ICellStyle>();

            for (int i = 0, cellIndex = startCellIndex; i < count; i++, cellIndex++)
            {
                ICell cell = row.GetCell(cellIndex);
                if (cell != null)
                {
                    ICellStyle style = cell.CellStyle;
                    if (style != null)
                    {
                        colStyleMap.Add(cellIndex, style);
                    }
                }
            }
            return colStyleMap;
        }
    }
}
