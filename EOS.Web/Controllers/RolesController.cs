﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;

namespace EOS.Web.Controllers
{
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly EOSContext _context;

        public RolesController(EOSContext context, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _roleManager = roleManager;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var roles = (from a in _context.Roles
                             select a);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    roles = roles.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    roles = roles.Where(m => m.Name.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = roles.Count();
                //Paging   
                var data = roles.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(string id = "")
        {
            if (id == "" || id == null)//flagged as insert
            {
                return View(new IdentityRole());
            }
            else
            {
                var role = await _context.Roles.FindAsync(id);
                return View(role);
            }
        }

        // POST: Sections/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(string id, [Bind("Name")] IdentityRole identityRole)
        {
            if (ModelState.IsValid)
            {
                if (RoleExists(id)) //update
                {
                    try
                    {
                        var rl = await _roleManager.FindByIdAsync(id);
                        rl.Name = identityRole.Name;
                        var result = await _roleManager.UpdateAsync(rl);
                        if (result.Succeeded)
                        {
                            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.Roles.ToList()) });
                        }
                        else
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", identityRole) });
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!RoleExists(identityRole.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else //new
                {
                    //_context.Add(identityRole);
                    var result = await _roleManager.CreateAsync(identityRole);
                    if (result.Succeeded)
                    {
                        return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.Roles.ToList()) });
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", identityRole) });
                    }
                }
            }
            else { 
                return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", identityRole) });
            }
        }

        private bool RoleExists(string id)
        {
            return _context.Roles.Any(e => e.Id == id);
        }
    }
}
