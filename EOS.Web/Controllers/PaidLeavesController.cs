﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using EOS.Web.Services;

namespace EOS.Web.Controllers
{
    public class PaidLeavesController : Controller
    {
        private readonly EOSContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly IMailer _mailer;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public PaidLeavesController(EOSContext context, UserManager<AppUser> userManager, IMailer mailer, IWebHostEnvironment env)
        {
            _context = context;
            _userManager = userManager;
            _mailer = mailer;
            _hostingEnvironment = env;
        }

        // GET: PaidLeaves
        [Authorize]
        public IActionResult Index()
        {
            ViewBag.Type = _context.Type.ToList();
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var periode = _context.PaidLeavePeriod.Where(p => p.UserId == userId.Value.ToString());
            ViewBag.Periode = periode.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data 
                var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
                //var paidLeave = (from p in _context.PaidLeave.Where(r => r.Requestor.Id.Equals(userId.Value))
                //                 select p);
                var paidLeave = (from p in _context.PaidLeave join u in _context.AppUsers on p.RequestorId equals u.Id join t in _context.Type on p.TypeId equals t.Id where(p.Requestor.Id.Equals(userId.Value) && (p.SubmissionType.Equals("Cuti")))
                                 select new { p.PaidLeaveId, p.StartDate, p.EndDate, p.TypeId, t.PaidLeaveName, p.Remarks, p.Status, p.RequestorId, p.RequestDate, p.Value, p.FirstApprover, p.FirstApproverResponse, p.FirstApproverTimestamp, p.SecondApprover, p.SecondApproverResponse, p.SecondApproverTimestamp, u.FullName, p.PaidLeavePhoto});
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    paidLeave = paidLeave.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    paidLeave = paidLeave.Where(p => p.Remarks.Contains(searchValue) || p.Status.Contains(searchValue) || p.FullName.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = paidLeave.Count();
                //Paging   
                var data = paidLeave.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: PaidLeaves/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(int id = 0)
        {
            if (id == 0)//flagged as insert
            {
                var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
                //tipe
                ViewBag.Type = _context.Type.ToList();
                //periode
                ViewBag.Periode = _context.PaidLeavePeriod.ToList();
                //populate
                PopulateTypeDropDownList();
                PopulatePaidLeavePeriod();
                return View(new PaidLeave());
            }
            else
            {
                var paidLeave = await _context.PaidLeave.FindAsync(id);
                PopulateTypeDropDownList(paidLeave.Type);
                PopulatePaidLeavePeriod(paidLeave.Period);
                if (paidLeave == null)
                {
                    return NotFound();
                }
                return View(paidLeave);
            }
        }

        // POST: PaidLeaves/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("StartDate,EndDate,TypeId,PeriodId,Remarks,Value")] PaidLeave paidLeave, List<IFormFile> PaidLeavePhoto)
        {
            if (ModelState.IsValid && Validation(paidLeave))
            {
                if (id == 0) //add new cuti
                {
                    paidLeave.RequestDate = DateTime.Now;
                    paidLeave.Status = "Submitted";
                    paidLeave.SubmissionType = "Cuti";
                    paidLeave.EndDate = paidLeave.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    AppUser applicationUser = await _userManager.GetUserAsync(User);
                    paidLeave.Requestor = applicationUser;
                    var type = await _context.Type.Where(x => x.Id == paidLeave.TypeId).SingleOrDefaultAsync();
                    if(type.PaidLeaveName == "Sakit" && PaidLeavePhoto.Count > 0)
                    {
                        paidLeave.Value = 0;
                        foreach (var file in PaidLeavePhoto)
                        {
                            using (var dataStream = new MemoryStream())
                            {
                                await file.CopyToAsync(dataStream);
                                paidLeave.PaidLeavePhoto = dataStream.ToArray();
                            }
                        }
                        
                    }
                    //check role first
                    var checkRoleApprover = await _userManager.IsInRoleAsync(applicationUser, "Approver");
                    if (checkRoleApprover)
                    {
                        //if bod, auto completed
                        var checkBoD = await _userManager.IsInRoleAsync(applicationUser, "BoD");
                        var isDepartmentHead = await _context.Department.Where(x => x.DepartmentHead == applicationUser.Email).FirstOrDefaultAsync();
                        var isSectionHead = await _context.Section.Where(x => x.SectionHead == applicationUser.Email).FirstOrDefaultAsync();
                        if (checkBoD) //is BoD, auto completed
                        {
                            DateTime now = DateTime.Now;
                            paidLeave.Status = "Completed";
                            paidLeave.FirstApprover = applicationUser.Email;
                            paidLeave.FirstApproverResponse = "Approve";
                            paidLeave.FirstApproverTimestamp = now;
                            paidLeave.SecondApprover = applicationUser.Email;
                            paidLeave.SecondApproverResponse = "Approve";
                            paidLeave.SecondApproverTimestamp = now;
                            _context.Add(paidLeave);
                            await _context.SaveChangesAsync();
                            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
                        }
                        //if manager, need approval bod
                        else if(isDepartmentHead != null) //manager, need BoD Approval
                        {
                            //find bod
                            var bod = await (from user in _context.AppUsers
                                             join userRole in _context.UserRoles
                                             on user.Id equals userRole.UserId
                                             join role in _context.Roles
                                             on userRole.RoleId equals role.Id
                                             where role.Name == "BoD"
                                             select user).ToListAsync();
                            List<string> emailList = new List<string>();
                            foreach (AppUser b in bod)
                            {
                                emailList.Add(b.Email);
                            }
                            string email = string.Join(",", emailList);
                            paidLeave.FirstApprover = email;
                            paidLeave.SecondApprover = email;
                            _context.Add(paidLeave);
                            await _context.SaveChangesAsync();
                            //send email
                            if (_hostingEnvironment.IsDevelopment())
                            {
                                await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                            }
                            else
                            {
                                await _mailer.SendEmailAsync(email, string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                            }
                            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
                        }
                        //if section head, need approval manager
                        else if(isSectionHead != null)
                        {
                            var departmentHead = await _context.Department.Where(x => x.DepartmentId == applicationUser.DepartmentId)
                                .SingleOrDefaultAsync();
                            paidLeave.FirstApprover = departmentHead.DepartmentHead;
                            _context.Add(paidLeave);
                            await _context.SaveChangesAsync();
                            if (_hostingEnvironment.IsDevelopment())
                            {
                                await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                            }
                            else
                            { //production
                                await _mailer.SendEmailAsync(paidLeave.FirstApprover, string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                            }
                            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
                        }
                        else
                        {
                            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "_ViewAll", paidLeave) });
                        }

                    }
                    else //send to section head
                    {
                        var sectionHead = await _context.Section.Where(x => x.SectionId == applicationUser.SectionId)
                                .SingleOrDefaultAsync();
                        paidLeave.FirstApprover = sectionHead.SectionHead;
                        _context.Add(paidLeave);
                        await _context.SaveChangesAsync();
                        if (_hostingEnvironment.IsDevelopment())
                        {
                            await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                        }
                        else
                        {
                            await _mailer.SendEmailAsync(paidLeave.FirstApprover, string.Format("[EOS] Pengajuan Cuti {0}", applicationUser.FullName), GenerateNewRequestBody(paidLeave.FirstApprover, paidLeave));
                        }
                        return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
                    }
                }
                else //update
                {
                    try
                    {
                        _context.Update(paidLeave);
                        await _context.SaveChangesAsync();
                        return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!PaidLeaveExists(paidLeave.PaidLeaveId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                PopulatePaidLeavePeriod();
                PopulateTypeDropDownList();
                return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", paidLeave) });
            }
        }

        // POST: PaidLeaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paidLeave = await _context.PaidLeave.FindAsync(id);
            _context.PaidLeave.Remove(paidLeave);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
        }

        // GET: PaidLeaves/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> Details(int id)
        {
            ViewBag.Type = _context.Type.ToList();
            var model = (from p in _context.PaidLeave
                         join u in _context.AppUsers on p.RequestorId equals u.Id
                         join t in _context.Type on p.TypeId equals t.Id
                         where (p.PaidLeaveId.Equals(id))
                         select new ViewPaidLeave(p.PaidLeaveId, p.StartDate, p.EndDate, p.TypeId, t.PaidLeaveName, p.Remarks, p.Status, p.RequestorId, p.RequestDate, p.Value, p.FirstApprover, p.FirstApproverResponse, p.FirstApproverTimestamp, p.SecondApprover, p.SecondApproverResponse, p.SecondApproverTimestamp, u.FullName));

            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "Details", model) });
        }

        private bool PaidLeaveExists(int id)
        {
            return _context.PaidLeave.Any(e => e.PaidLeaveId == id);
        }

        private void PopulateTypeDropDownList(object selectedType = null)
        {
            var typeQuery = from d in _context.Type
                                   orderby d.Id
                                   select d;
            List<SelectListItem> items = new SelectList(typeQuery, "Id", "NameValue", selectedType).ToList();
            ViewBag.Type = new SelectList(items, "Value", "Text");
        }

        private void PopulatePaidLeavePeriod(object selectedType = null)
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var typeQuery = from d in _context.PaidLeavePeriod
                            where d.UserId.Equals(userId.Value) && d.Status.Equals(1) && d.EndDate > DateTime.Today 
                            select d;
            List<SelectListItem> items = new SelectList(typeQuery, "Id", "PeriodName", selectedType).ToList();
            ViewBag.Period = new SelectList(items, "Value", "Text");
        }

        public string GenerateNewRequestBody(string approverName, PaidLeave details)
        {
            var type = _context.Type.Find(details.TypeId);
            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("Dear {0},<br><br>", approverName);
            msg.AppendFormat("{0} mengajukan cuti dengan detail sebagai berikut :<br>", details.Requestor.FullName);
            msg.AppendFormat("Tanggal Cuti      : {0} - {1}<br>", details.StartDate, details.EndDate);
            msg.AppendFormat("Deskripsi Cuti    : {0} - {1}<br>", type.PaidLeaveName, details.Remarks);
            msg.AppendFormat("Total Cuti        : {0} Hari<br>", details.Value);
            msg.Append("<br><br>");
            msg.Append("Membutuhkan persetujuan (approval) anda terlebih dahulu, silahkan kunjungi aplikasi EOS.");
            return msg.ToString();
        }

        public JsonResult GetRemainingLeaves(int period)
        {
            var remainingLeaves = _context.ViewRemainingLeaves.Where(p => p.Id == period);
            if (remainingLeaves.Count() == 0)
            {
                var r = _context.PaidLeavePeriod.Where(p => p.Id == period);
                return new JsonResult(r);
            }
            else
            {
                return new JsonResult(remainingLeaves);
            }
        }

        [AcceptVerbs("GET", "POST")]
        public IActionResult VerifyLimitPeriod(double value, int periodId)
        {
            double val = value;
            if (!ModelState.IsValid)
            {
                var remainingLeaves = _context.ViewRemainingLeaves.Where(p => p.Id == periodId);
            }
            return Json(false);
        }

        public bool Validation(PaidLeave paidLeave)
        {
            var remainingLeaves = _context.ViewRemainingLeaves.Where(p => p.Id == paidLeave.PeriodId);
            ViewRemainingLeave[] vr = remainingLeaves.ToArray();
            if (vr.Length != 0)
            { 
                if(paidLeave.Value > vr[0].RemainingLeave)
                {
                    // add validasi melebihi
                    ModelState.AddModelError("Value", "Jumlah pengajuan cuti melebihi sisa cuti untuk periode ini.");
                    return false;
                }
            }
            else
            {
                var remain = _context.PaidLeavePeriod.Where(p => p.Id == paidLeave.PeriodId);
                PaidLeavePeriod[] plp = remain.ToArray();
                if(paidLeave.Value > plp[0].Value)
                {
                    ModelState.AddModelError("Value", "Jumlah pengajuan cuti melebihi sisa cuti untuk periode ini.");
                    return false;
                }
            }
            if(paidLeave.StartDate > paidLeave.EndDate)
            {
                ModelState.AddModelError("Value", "Tanggal mulai pengajuan tidak boleh melebihi tanggal selesai pengajuan.");
                return false;
            }
            return true;
        }
    }
}
