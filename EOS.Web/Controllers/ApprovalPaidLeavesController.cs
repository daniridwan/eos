﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity.UI.Services;
using System.Security.Claims;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using EOS.Web.Services;

namespace EOS.Web.Controllers
{
    public class ApprovalPaidLeavesController : Controller
    {
        private readonly EOSContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly IMailer _mailer;
        private readonly IWebHostEnvironment _hostingEnvironment;

        public ApprovalPaidLeavesController(EOSContext context, UserManager<AppUser> userManager, IMailer mailer, IWebHostEnvironment env)
        {
            _context = context;
            _userManager = userManager;
            _mailer = mailer;
            _hostingEnvironment = env;
        }

        // GET: ApprovalPaidLeaves
        [Authorize]
        public IActionResult Index()
        {
            ViewBag.Type = _context.Type.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data 
                var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
                var currUser = _context.AppUsers.Find(userId.Value);
                var paidLeave = (from p in _context.PaidLeave
                                 join u in _context.AppUsers on p.RequestorId equals u.Id
                                 join t in _context.Type on p.TypeId equals t.Id
                                 where ((p.FirstApprover.Contains(currUser.Email) && p.FirstApproverResponse == null)
                                 || (p.SecondApprover.Contains(currUser.Email) && p.SecondApproverResponse == null) && p.Status != "Completed")
                                 select new { p.PaidLeaveId, p.StartDate, p.EndDate, p.TypeId, t.PaidLeaveName, p.Remarks, p.Status, p.RequestorId, p.RequestDate, p.Value, p.FirstApprover, p.FirstApproverResponse, p.FirstApproverTimestamp, p.SecondApprover, p.SecondApproverResponse, p.SecondApproverTimestamp, u.FullName });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    paidLeave = paidLeave.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    paidLeave = paidLeave.Where(p => p.Remarks.Contains(searchValue) || p.Status.Contains(searchValue) || p.FullName.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = paidLeave.Count();
                //Paging   
                var data = paidLeave.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: ApprovalPaidLeaves/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> Approval(int id)
        {
            if (id != 0)//flagged as insert
            {
                var paidLeave = await _context.PaidLeave.FindAsync(id);
                PopulateTypeDropDownList(paidLeave.Type);
                if (paidLeave == null)
                {
                    return NotFound();
                }
                return View(paidLeave);
            }
            else
            {
                return NotFound();
            }
        }

        // POST: Sections/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Approval(int id, [Bind("Fullname,StartDate,EndDate,Response")] ViewPaidLeave paidLeave)
        {
            if (ModelState.IsValid)
            {
                if (id != 0) //update approval cuti
                {
                    try
                    {
                        //current user
                        var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
                        var currUser = _context.AppUsers.Find(userId.Value);

                        var orig = await _context.PaidLeave.FindAsync(id);
                        if (paidLeave.Response == "Approve")
                        {
                            //update cuti
                            if (orig.Status == "Submitted") //first approver
                            {
                                orig.FirstApprover = currUser.Email;
                                orig.Status = "In Process";
                                orig.FirstApproverResponse = "Approve";
                                orig.FirstApproverTimestamp = DateTime.Now;
                                //find 2nd approver
                                var requestor = await _context.AppUsers.FindAsync(orig.RequestorId);
                                var isDepartmentHead = await _context.Department.Where(x => x.DepartmentHead == requestor.Email).FirstOrDefaultAsync();
                                var isSectionHead = await _context.Section.Where(x => x.SectionHead == requestor.Email).FirstOrDefaultAsync();
                                var departmentHead = await _context.Department.Where(x => x.DepartmentId == requestor.DepartmentId).FirstOrDefaultAsync();
                                if (orig.SecondApprover != null) { 
                                    orig.SecondApprover = orig.SecondApprover.Replace(currUser.Email, "");
                                }
                                else
                                {
                                    //check if section head, send 2nd approval to bod
                                    if(isSectionHead != null)
                                    {
                                        // find bod
                                        var bod = await (from user in _context.AppUsers
                                              join userRole in _context.UserRoles
                                              on user.Id equals userRole.UserId
                                              join role in _context.Roles
                                              on userRole.RoleId equals role.Id
                                              where role.Name == "BoD"
                                              select user).ToListAsync();
                                        List<string> emailList = new List<string>();
                                        foreach (AppUser b in bod)
                                        {
                                            emailList.Add(b.Email);
                                        }
                                        string email = string.Join(" , ", emailList);
                                        orig.SecondApprover = email;
                                    }
                                    else 
                                    { 
                                        orig.SecondApprover = departmentHead.DepartmentHead;
                                    }
                                }
                                if (orig.FirstApprover == orig.SecondApprover)
                                {
                                    orig.SecondApproverResponse = orig.FirstApproverResponse;
                                    orig.SecondApproverTimestamp = orig.FirstApproverTimestamp;
                                    orig.Status = "Completed";
                                }
                                _context.Update(orig);
                                await _context.SaveChangesAsync();
                                //send email to 2nd approver
                                if (_hostingEnvironment.IsDevelopment())
                                {
                                    if (orig.FirstApprover != orig.SecondApprover)
                                    {
                                        await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateSecondApproverEmailBody(orig.SecondApprover, orig));
                                    }
                                    else
                                    {
                                        await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));
                                    }
                                }
                                else
                                {
                                    if (orig.FirstApprover != orig.SecondApprover){
                                        await _mailer.SendEmailAsync(orig.SecondApprover, string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateSecondApproverEmailBody(orig.SecondApprover, orig));
                                    }
                                    else {
                                        //send to requestor
                                        await _mailer.SendEmailAsync(requestor.Email, string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));
                                    }
                                }
                            }
                            else if (orig.Status == "In Process") //second approver
                            {
                                orig.SecondApprover = currUser.Email;
                                orig.Status = "Completed";
                                orig.SecondApproverResponse = "Approve";
                                orig.SecondApproverTimestamp = DateTime.Now;
                                //find requestor
                                var requestor = await _context.AppUsers.FindAsync(orig.RequestorId);
                                _context.Update(orig);
                                await _context.SaveChangesAsync();
                                //send email to requestor
                                if (_hostingEnvironment.IsDevelopment())
                                {
                                    await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));   
                                }
                                else
                                {
                                    await _mailer.SendEmailAsync(requestor.Email, string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));
                                }
                            }
                            else
                            {
                                // do nothing
                            }
                            
                        }
                        else
                        {
                            //update cuti -> cancelled
                            if (orig.Status == "Submitted")
                            {
                                orig.FirstApprover = currUser.Email;
                                orig.Status = "Rejected";
                                orig.FirstApproverResponse = "Reject";
                                orig.FirstApproverTimestamp = DateTime.Now;
                            }
                            else {
                                orig.SecondApprover = currUser.Email;
                                orig.Status = "Rejected";
                                orig.SecondApproverResponse = "Reject";
                                orig.SecondApproverTimestamp = DateTime.Now;
                            }
                            _context.Update(orig);
                            await _context.SaveChangesAsync();
                            //find requestor
                            var requestor = await _context.AppUsers.FindAsync(orig.RequestorId);
                            //send email to requestor
                            if (_hostingEnvironment.IsDevelopment())
                            {
                                await _mailer.SendEmailAsync("dani.ridwan@redeco-rpu.com", string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));
                            }
                            else
                            {
                                await _mailer.SendEmailAsync(requestor.Email, string.Format("[EOS] Pengajuan Cuti {0}", requestor.FullName), GenerateRequestorEmailBody(orig, paidLeave.Response));
                            }
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!PaidLeaveExists(paidLeave.PaidLeaveId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else 
                {
                    
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
            }
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "Details", paidLeave) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Cancel(int id, [Bind("StartDate,EndDate,TypeId,Remarks,Value")] PaidLeave paidLeave)
        {
            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "Details", paidLeave) });
        }

        // POST: PaidLeaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paidLeave = await _context.PaidLeave.FindAsync(id);
            _context.PaidLeave.Remove(paidLeave);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeave.ToList()) });
        }

        private bool PaidLeaveExists(int id)
        {
            return _context.PaidLeave.Any(e => e.PaidLeaveId == id);
        }

        private void PopulateTypeDropDownList(object selectedType = null)
        {
            var typeQuery = from d in _context.Type
                            orderby d.Id
                            select d;
            ViewBag.Type = new SelectList(typeQuery, "Id", "PaidLeaveName", selectedType);
        }

        [NoDirectAccess]
        public async Task<IActionResult> Details(int id)
        {
            var model = (from p in _context.PaidLeave
                            join u in _context.AppUsers on p.RequestorId equals u.Id
                            join t in _context.Type on p.TypeId equals t.Id
                            where (p.PaidLeaveId.Equals(id))
                            select new ViewPaidLeave ( p.PaidLeaveId, p.StartDate, p.EndDate, p.TypeId, t.PaidLeaveName, p.Remarks, p.Status, p.RequestorId, p.RequestDate, p.Value, p.FirstApprover, p.FirstApproverResponse, p.FirstApproverTimestamp, p.SecondApprover, p.SecondApproverResponse, p.SecondApproverTimestamp, u.FullName ));

            return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "Details", model) });
        }

        public string GenerateSecondApproverEmailBody(string approverName, PaidLeave details)
        {
            var type = _context.Type.Find(details.TypeId);
            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("Dear {0},<br><br>", approverName);
            msg.AppendFormat("{0} mengajukan cuti dengan detail sebagai berikut :<br>", details.Requestor.FullName);
            msg.AppendFormat("Tanggal Cuti      : {0} - {1}<br>", details.StartDate, details.EndDate);
            msg.AppendFormat("Deskripsi Cuti    : {0} - {1}<br>", type.PaidLeaveName, details.Remarks);
            msg.AppendFormat("Total Cuti        : {0} Hari<br>", details.Value);
            msg.Append("<br>");
            msg.Append("Membutuhkan persetujuan (approval) anda terlebih dahulu, silahkan kunjungi aplikasi EOS.");
            return msg.ToString();
        }

        public string GenerateRequestorEmailBody(PaidLeave details, string respon)
        {
            var type = _context.Type.Find(details.TypeId);
            StringBuilder msg = new StringBuilder();
            msg.AppendFormat("Dear {0},<br><br>", details.Requestor.FullName);
            msg.Append("Pengajuan cuti anda dengan detail sebagai berikut :<br>");
            msg.AppendFormat("Tanggal Cuti      : {0} - {1}<br>", details.StartDate, details.EndDate);
            msg.AppendFormat("Deskripsi Cuti    : {0} - {1}<br>", type.PaidLeaveName, details.Remarks);
            msg.AppendFormat("Total Cuti        : {0} Hari<br>", details.Value);
            msg.Append("<br>");
            msg.AppendFormat("Telah di {0}",respon);
            return msg.ToString();
        }
    }
}
