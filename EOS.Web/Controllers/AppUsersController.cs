﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace EOS.Web.Controllers
{
    public class AppUsersController : Controller
    {
        private readonly EOSContext _context;
        private readonly UserManager<AppUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public AppUsersController(EOSContext context, UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        // GET: Banks
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var appUsers = (from a in _context.AppUsers
                            select a);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    appUsers = appUsers.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    appUsers = appUsers.Where(m => m.FullName.Contains(searchValue) || m.Email.Contains(searchValue) || 
                    m.PhoneNumber.Contains(searchValue) || m.EmployeeNumber.Contains(searchValue) || m.Address.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = appUsers.Count();
                //Paging   
                var data = appUsers.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Banks/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(string id = "")
        {
            if (id == "" || id == null)//flagged as insert
            {
                PopulateSectionDropDownList();
                PopulateDepartmentDropDownList();
                PopulateDivisionDropDownList();
                PopulateJobDropDownList();
                PopulateGradeDropDownList();
                //PopulateBankDropDownList();
                UserRole usr = new UserRole();
                usr.ApplicationUser = new AppUser();
                usr.ApplicationRoles = await _roleManager.Roles.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id
                }).ToListAsync();
                return View(usr);
            }
            else
            {
                UserRole userRole = new UserRole();
                //var appUser = await _context.AppUsers.FindAsync(id);
                var appUser = _context.Users.Where(x => x.Id.Equals(id)).SingleOrDefault();
                var userInRole = _context.UserRoles.Where(x => x.UserId.Equals(id)).Select(x => x.RoleId).ToList();
                userRole.ApplicationUser = appUser;
                userRole.ApplicationRoles = await _roleManager.Roles.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id,
                    Selected = userInRole.Contains(x.Id)
                }).ToListAsync();
                PopulateSectionDropDownList(appUser.Section);
                PopulateDepartmentDropDownList(appUser.Department);
                PopulateDivisionDropDownList(appUser.Division);
                PopulateJobDropDownList(appUser.Job);
                PopulateGradeDropDownList(appUser.Grade);
                //PopulateBankDropDownList(appUser.Bank);
                if (appUser == null)
                {
                    return NotFound();
                }
                return View(userRole);
            }
        }

        // POST: AppUsers/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> AddOrEdit(string id, [Bind("UserName,Email,PhoneNumber,FullName,BirthPlace,BirthDate,EmployeeNumber,StartJoinDate,EndJoinDate,Address,SectionId,DepartmentId,DivisionId,JobId,GradeId,BankId")] UserRole appUser)
        public async Task<IActionResult> AddOrEdit(string id, UserRole userRole)
        {
            if (ModelState.IsValid)
            {
                if (IdExists(id)) //update
                {
                    try {
                        var usr = await _userManager.FindByIdAsync(id);
                        var selectedRoleId = userRole.ApplicationRoles.Where(x => x.Selected).Select(x => x.Value);
                        var alreadyExists = _context.UserRoles.Where(x => x.UserId.Equals(id)).Select(x => x.RoleId).ToList();
                        var toAdd = selectedRoleId.Except(alreadyExists);
                        var toRemove = alreadyExists.Except(selectedRoleId);

                        //update app users
                        usr.FullName = userRole.ApplicationUser.FullName;
                        usr.UserName = userRole.ApplicationUser.UserName;
                        usr.StartJoinDate = userRole.ApplicationUser.StartJoinDate;
                        usr.Email = userRole.ApplicationUser.Email;
                        usr.EmployeeNumber = userRole.ApplicationUser.EmployeeNumber;
                        usr.SectionId = userRole.ApplicationUser.SectionId;
                        usr.DepartmentId = userRole.ApplicationUser.DepartmentId;
                        usr.DivisionId = userRole.ApplicationUser.DivisionId;
                        usr.JobId = userRole.ApplicationUser.JobId;
                        usr.GradeId = userRole.ApplicationUser.GradeId;
                        usr.BirthPlace = userRole.ApplicationUser.BirthPlace;
                        usr.BirthDate = userRole.ApplicationUser.BirthDate;
                        usr.EmployeeType = userRole.ApplicationUser.EmployeeType;
                        usr.SyncId = userRole.ApplicationUser.SyncId;

                        foreach (var item in toRemove)
                        {
                            _context.UserRoles.Remove(new IdentityUserRole<string>
                            {
                                RoleId = item,
                                UserId = usr.Id
                            });
                        }
                        foreach (var item in toAdd)
                        {
                            _context.UserRoles.Add(new IdentityUserRole<string>
                            {
                                RoleId = item,
                                UserId = usr.Id
                            });
                        }

                        var update = await _userManager.UpdateAsync(usr);

                        if (!update.Succeeded)
                        {
                            foreach (var error in update.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UsernameExists(userRole.ApplicationUser.UserName))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else //create new
                {
                    userRole.ApplicationUser.EmailConfirmed = true;
                    var selectedRoleId = userRole.ApplicationRoles.Where(x => x.Selected).Select(x => x.Value);

                    foreach (var item in selectedRoleId)
                    {
                        _context.UserRoles.Add(new IdentityUserRole<string>
                        {
                            RoleId = item,
                            UserId = userRole.ApplicationUser.Id
                        });
                    }

                    var result = await _userManager.CreateAsync(userRole.ApplicationUser, userRole.ApplicationUser.UserName);

                    if (result.Succeeded)
                    {
                        return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.AppUsers.ToList()) });
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                        PopulateSectionDropDownList(userRole.ApplicationUser.Section);
                        PopulateDepartmentDropDownList(userRole.ApplicationUser.Department);
                        PopulateDivisionDropDownList(userRole.ApplicationUser.Division);
                        PopulateJobDropDownList(userRole.ApplicationUser.Job);
                        PopulateGradeDropDownList(userRole.ApplicationUser.Grade);
                        return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", userRole.ApplicationUser) });
                    }

                    //appUser.EmailConfirmed = true;
                    //var result = await _userManager.CreateAsync(appUser, appUser.UserName);
                    //if(result.Succeeded)
                    //{
                    //    return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.AppUsers.ToList() )});
                    //}
                    //else
                    //{
                    //    foreach (var error in result.Errors)
                    //    {
                    //        ModelState.AddModelError(string.Empty, error.Description);
                    //    }
                    //    PopulateSectionDropDownList(appUser.Section);
                    //    PopulateDepartmentDropDownList(appUser.Department);
                    //    PopulateDivisionDropDownList(appUser.Division);
                    //    PopulateJobDropDownList(appUser.Job);
                    //    PopulateGradeDropDownList(appUser.Grade);
                    //    return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", appUser) });
                    //}
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.AppUsers.ToList() )});
            }
            else {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", userRole) });
            }
        }

        // POST: Banks/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appUser = await _context.AppUsers.FindAsync(id);
            _context.AppUsers.Remove(appUser);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", await PaginatedList<AppUser>.CreateAsync(_context.AppUsers.AsNoTracking(), 1, 15)) });
        }

        [NoDirectAccess]
        public async Task<IActionResult> SetInactive(string id = "")
        {
            UserRole userRole = new UserRole();
            var appUser = _context.Users.Where(x => x.Id.Equals(id)).SingleOrDefault();
            var userInRole = _context.UserRoles.Where(x => x.UserId.Equals(id)).Select(x => x.RoleId).ToList();
            userRole.ApplicationUser = appUser;
            userRole.ApplicationRoles = await _roleManager.Roles.Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id,
                Selected = userInRole.Contains(x.Id)
            }).ToListAsync();
            if (appUser == null)
            {
                return NotFound();
            }
            return View(userRole);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SetInactive(string id, UserRole userRole)
        {
            if (Validation(userRole))
            {
                if (IdExists(id)) //update
                {
                    try
                    {
                        var usr = await _userManager.FindByIdAsync(id);

                        //set inactive app users
                        usr.EndJoinDate = userRole.ApplicationUser.EndJoinDate;
                        usr.LockoutEnd = new DateTime(2099, 12, 31);
                        var update = await _userManager.UpdateAsync(usr);

                        if (!update.Succeeded)
                        {
                            foreach (var error in update.Errors)
                            {
                                ModelState.AddModelError(string.Empty, error.Description);
                            }
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UsernameExists(userRole.ApplicationUser.UserName))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.AppUsers.ToList()) });
            }
            else
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "SetInactive", userRole) });
            }
        }

        [HttpPost, ActionName("ResetPassword")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPasswordConfirmed(string id)
        {
            var appUser = await _userManager.FindByIdAsync(id);
            var token = await _userManager.GeneratePasswordResetTokenAsync(appUser);
            var result = await _userManager.ResetPasswordAsync(appUser, token, appUser.UserName);
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.AppUsers.ToList()) });
        }

        private bool UsernameExists(string username)
        {
            return _context.AppUsers.Any(e => e.UserName == username);
        }        

        private bool IdExists(string id)
        {
            return _context.AppUsers.Any(e => e.Id == id);
        }

        private void PopulateSectionDropDownList(object selectedSection = null)
        {
            var sectionQuery = from d in _context.Section
                            orderby d.SectionId
                            select d;
            ViewBag.Section = new SelectList(sectionQuery, "SectionId", "SectionName", selectedSection);
        }

        private void PopulateDepartmentDropDownList(object selectedDepartment = null)
        {
            var departmentQuery = from d in _context.Department
                               orderby d.DepartmentId
                               select d;
            ViewBag.Department = new SelectList(departmentQuery, "DepartmentId", "DepartmentName", selectedDepartment);
        }

        private void PopulateDivisionDropDownList(object selectedDivision = null)
        {
            var divisionQuery = from d in _context.Division
                                  orderby d.DivisionId
                                  select d;
            ViewBag.Division = new SelectList(divisionQuery, "DivisionId", "DivisionName", selectedDivision);
        }

        private void PopulateJobDropDownList(object selectedJob = null)
        {
            var jobQuery = from d in _context.Job
                                orderby d.JobId
                                select d;
            ViewBag.Job = new SelectList(jobQuery, "JobId", "JobTitle", selectedJob);
        }

        private void PopulateGradeDropDownList(object selectedGrade = null)
        {
            var gradeQuery = from d in _context.Grade
                           orderby d.GradeId
                           select d;
            ViewBag.Grade = new SelectList(gradeQuery, "GradeId", "GradeName", selectedGrade);
        }

        private void PopulateBankDropDownList(object selectedBank = null)
        {
            var bankQuery = from d in _context.Bank
                             orderby d.BankId
                             select d;
            ViewBag.Bank = new SelectList(bankQuery, "BankId", "BankName", selectedBank);
        }

        public bool Validation(UserRole userRole)
        {
            if (userRole.ApplicationUser.EndJoinDate == DateTime.MinValue || userRole.ApplicationUser.EndJoinDate == null)
            {
                // add validasi melebihi
                ModelState.AddModelError("Value", "Silahkan isi tanggal terakhir bekerja");
                return false;
            }
            return true;
        }
    }
}
