﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace EOS.Web.Controllers
{
    public class DepartmentsController : Controller
    {
        private readonly EOSContext _context;

        public DepartmentsController(EOSContext context)
        {
            _context = context;
        }

        // GET: Sections
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var department = (from s in _context.Department
                               select s);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    department = department.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    department = department.Where(m => m.DepartmentName.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = department.Count();
                //Paging   
                var data = department.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Departments/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(int id = 0)
        {
            if (id == 0)//flagged as insert
            {
                PopulateDepartmentHeadDropDownList();
                return View(new Department());
            }
            else
            {
                var department = await _context.Department.FindAsync(id);
                var departmentHead = await _context.AppUsers.FindAsync(department.DepartmentHead);
                PopulateDepartmentHeadDropDownList(departmentHead);
                if (department == null)
                {
                    return NotFound();
                }
                return View(department);
            }
        }

        // POST: Departments/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("DepartmentId,DepartmentName,DepartmentHead")] Department department)
        {
            if (ModelState.IsValid)
            {
                if (id == 0) //add new
                {
                    _context.Add(department);
                    await _context.SaveChangesAsync();
                }
                else //update
                {
                    try
                    {
                        _context.Update(department);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!DepartmentExists(department.DepartmentId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.Department.ToList()) });
            }
            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", department) });
        }

        // POST: Sections/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var department = await _context.Department.FindAsync(id);
            _context.Department.Remove(department);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.Department.ToList()) });
        }

        private bool DepartmentExists(int id)
        {
            return _context.Department.Any(e => e.DepartmentId == id);
        }

        private void PopulateDepartmentHeadDropDownList(object selectedDepartmentHead = null)
        {
            var departmentHeadQuery = from s in _context.AppUsers
                                   orderby s.Id
                                   select s;
            ViewBag.DepartmentHead = new SelectList(departmentHeadQuery, "Email", "FullName", selectedDepartmentHead);
        }
    }
}
