﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace EOS.Web.Controllers
{
    public class PaidLeavePeriodController : Controller
    {
        private readonly EOSContext _context;

        public PaidLeavePeriodController(EOSContext context)
        {
            _context = context;
        }

        // GET: PaidLeavePeriod
        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var paidLeavePeriod = (from p in _context.PaidLeavePeriod join u in _context.AppUsers on p.UserId equals u.Id
                            select new { p.Id, p.PeriodName, p.StartDate, p.EndDate, u.FullName, p.Value, p.StatusString });
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    paidLeavePeriod = paidLeavePeriod.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    paidLeavePeriod = paidLeavePeriod.Where(m => m.PeriodName.Contains(searchValue) || m.FullName.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = paidLeavePeriod.Count();
                //Paging   
                var data = paidLeavePeriod.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: PaidLeavePeriod/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(int id = 0)
        {
            if (id == 0)//flagged as insert
            {
                PopulateUser();
                return View(new PaidLeavePeriod());
            }
            else
            {
                var paidLeavePeriod = await _context.PaidLeavePeriod.FindAsync(id);
                PopulateUser(paidLeavePeriod.User);
                if (paidLeavePeriod == null)
                {
                    return NotFound();
                }
                return View(paidLeavePeriod);
            }
        }

        // POST: Banks/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("Id,UserId,PeriodName,StartDate,EndDate,Value,Status")] PaidLeavePeriod paidLeavePeriod)
        {
            if (ModelState.IsValid)
            {
                if (id == 0) //add new
                {
                    _context.Add(paidLeavePeriod);
                    await _context.SaveChangesAsync();
                }
                else //update
                {
                    try
                    {
                        _context.Update(paidLeavePeriod);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!PeriodExists(paidLeavePeriod.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeavePeriod.ToList()) });
            }
            else {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                PopulateUser();
                return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", paidLeavePeriod) });
            }
        }

        private bool PeriodExists(int id)
        {
            return _context.PaidLeavePeriod.Any(e => e.Id == id);
        }

        // POST: PaidLeavePeriod/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var paidLeavePeriod = await _context.PaidLeavePeriod.FindAsync(id);
            _context.PaidLeavePeriod.Remove(paidLeavePeriod);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.PaidLeavePeriod.ToList()) });
        }

        private void PopulateUser(object selectedType = null)
        {
            var userQuery = from d in _context.AppUsers
                            where d.EndJoinDate.Equals(DateTime.MinValue)
                            orderby d.Id
                            select d;
            List<SelectListItem> items = new SelectList(userQuery, "Id", "FullName", selectedType).ToList();
            ViewBag.User = new SelectList(items, "Value", "Text");
        }

        public JsonResult GetPeriodUser(string id)
        {
            var period = _context.PaidLeavePeriod.Where(p => p.UserId == id && p.Status.Equals(1));
            return new JsonResult(period);
        }
    }
}
