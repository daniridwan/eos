﻿using EOS.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using EOS.Web.Data;
using EOS.Web.Models;

namespace EOS.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly EOSContext _context;
        private readonly ILogger<HomeController> _logger;

        public HomeController(EOSContext context, ILogger<HomeController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public JsonResult GetRemainingLeaves()
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var remainingLeaves = _context.ViewRemainingLeaves.Where(p => p.UserId == userId.Value && p.EndDate > DateTime.Now).OrderBy(p => p.EndDate).ToList();
            if (remainingLeaves != null)
            {
                return new JsonResult(remainingLeaves);
            }
            else
            {
                var paidleavePeriod = _context.PaidLeavePeriod.Where(p => p.UserId == userId.Value && p.EndDate > DateTime.Now).OrderBy(p => p.EndDate).ToList();
                return new JsonResult(paidleavePeriod);
            }
        }

        public JsonResult GetWaitingApprovalLeaves()
        {
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var waitingApproval = _context.PaidLeave.Where(p => p.RequestorId == userId.Value && new[] { "Submitted", "In Process" }.Contains(p.Status));
            return new JsonResult(waitingApproval);
        }

        public JsonResult GetEvents()
        {
            var events = _context.ViewAgendaLists.Select(e => new
            {
                id = e.No,
                title = e.Name,
                description = e.Name,
                start = e.StartDate.ToString("MM/dd/yyyy HH:mm:ss"), //please dont change format
                //end = e.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("MM/dd/yyyy HH:mm:ss")
                end = e.EndDate.ToString("MM/dd/yyyy HH:mm:ss"),
                backgroundColor = e.Name.Contains("Cuti") || e.Name.Contains("Izin") ? "#233d89" : "#dc3545",
                textColor = "#ffffff"
            }).ToList();
            return new JsonResult(events);
        }

        public IActionResult LoadDataTM()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var attd = (from a in _context.AttdTran
                            join ap in _context.AppUsers on a.EnrollNumber equals ap.SyncId
                            where a.Source.Equals("193.168.1.90") && ap.DivisionId.Equals(2)
                            orderby a.DateIn descending
                            select new { a.AttdTranId, ap.FullName, a.DateIn, a.StateType });

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    attd = attd.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    attd = attd.Where(p => p.FullName.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = attd.Take(50).ToList().Count();
                //Paging   
                var data = attd.Skip(skip).Take(10).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IActionResult LoadDataHO()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                var attd = (from a in _context.AttdTran
                            join ap in _context.AppUsers on a.EnrollNumber equals ap.SyncId
                            where a.Source.Equals("192.168.1.90") && ap.DivisionId.Equals(1)
                            orderby a.DateIn descending
                            select new { a.AttdTranId, ap.FullName, a.DateIn, a.StateType });

                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    attd = attd.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    attd = attd.Where(p => p.FullName.Contains(searchValue));
                }
                //total number of rows counts   
                recordsTotal = attd.Take(50).ToList().Count();
                //Paging   
                var data = attd.Skip(skip).Take(10).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
