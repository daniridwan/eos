﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.Security.Claims;
using EOS.Web.Services;

namespace EOS.Web.Controllers
{
    public class PaidLeaveByHrController : Controller
    {
        private readonly EOSContext _context;

        public PaidLeaveByHrController(EOSContext context)
        {
            _context = context;
        }

        [Authorize]
        public ActionResult Index()
        {
            var emp = _context.AppUsers.Where(p => p.EndJoinDate != null);
            ViewBag.Employee = emp.ToList();
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var vrl = (from v in _context.ViewPaidLeaveDetails
                           select v);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    vrl = vrl.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    vrl = vrl.Where(v => v.FullName.Contains(searchValue) || v.PeriodName.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = vrl.Count();
                //Paging   
                var data = vrl.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: PaidLeaves/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(int id = 0)
        {
            if (id == 0)//flagged as insert
            {
                var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
                //tipe
                ViewBag.Type = _context.Type.ToList();
                //populate
                PopulateUser();
                return View(new PaidLeave());
            }
            else
            {
                var paidLeave = await _context.PaidLeave.FindAsync(id);
                PopulateUser(paidLeave.Period);
                if (paidLeave == null)
                {
                    return NotFound();
                }
                return View(paidLeave);
            }
        }

        private void PopulateUser(object selectedType = null)
        {
            //var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var typeQuery = from d in _context.AppUsers
                            where d.EndJoinDate.Equals(DateTime.MinValue)
                            orderby d.Id
                            select d;
            List<SelectListItem> items = new SelectList(typeQuery, "Id", "FullName", selectedType).ToList();
            ViewBag.Employee = new SelectList(items, "Value", "Text");
        }

        public JsonResult GetPeriodUser(string id)
        {
            var period = _context.PaidLeavePeriod.Where(p => p.UserId == id && p.Status != 1);
            return new JsonResult(period);
        }

    }
}
