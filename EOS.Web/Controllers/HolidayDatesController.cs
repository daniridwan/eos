﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EOS.Web.Data;
using EOS.Web.Models;
using EOS.Web.Areas.Identity.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;

namespace EOS.Web.Controllers
{
    public class HolidayDatesController : Controller
    {
        private readonly EOSContext _context;
        private readonly UserManager<AppUser> _userManager;

        public HolidayDatesController(EOSContext context, UserManager<AppUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData()
        {
            try
            {
                var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();// Skip number of Rows count  
                var length = Request.Form["length"].FirstOrDefault(); // Paging Length 10,20  
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault(); // Sort Column Name  
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();// Sort Column Direction (asc, desc)  
                var searchValue = Request.Form["search[value]"].FirstOrDefault(); // Search Value from (Search box)  
                int pageSize = length != null ? Convert.ToInt32(length) : 0; //Paging Size (10, 20, 50,100)  
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;

                // getting all data  
                var holidayDate = (from b in _context.HolidayDate
                                select b);
                //Sorting  
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    holidayDate = holidayDate.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                //Search  
                if (!string.IsNullOrEmpty(searchValue))
                {
                    holidayDate = holidayDate.Where(m => m.Name.Contains(searchValue));
                }

                //total number of rows counts   
                recordsTotal = holidayDate.Count();
                //Paging   
                var data = holidayDate.Skip(skip).Take(pageSize).ToList();
                //Returning Json Data  
                return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: Divisions/AddOrEdit
        [NoDirectAccess]
        public async Task<IActionResult> AddOrEdit(int id = 0)
        {
            if (id == 0)//flagged as insert
            {
                return View(new HolidayDate());
            }
            else
            {
                var holidayDate = await _context.HolidayDate.FindAsync(id);
                if (holidayDate == null)
                {
                    return NotFound();
                }
                return View(holidayDate);
            }
        }

        // POST: HolidayDates/AddOrEdit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("HolidayDateId,StartDate,EndDate,Name,Type")] HolidayDate holidayDate)
        {
            if (ModelState.IsValid)
            {
                if (id == 0) //add new
                {
                    holidayDate.EndDate = holidayDate.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                    _context.Add(holidayDate);
                    await _context.SaveChangesAsync();
                }
                else //update
                {
                    try
                    {
                        holidayDate.EndDate = holidayDate.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
                        _context.Update(holidayDate);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!HolidayDateExists(holidayDate.HolidayDateId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                return Json(new { isValid = true, html = Helper.RenderRazorViewString(this, "_ViewAll", _context.HolidayDate.ToList()) });
            }
            return Json(new { isValid = false, html = Helper.RenderRazorViewString(this, "AddOrEdit", holidayDate) });
        }

        // POST: HolidayDates/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var holidayDate = await _context.HolidayDate.FindAsync(id);
            _context.HolidayDate.Remove(holidayDate);
            await _context.SaveChangesAsync();
            return Json(new { html = Helper.RenderRazorViewString(this, "_ViewAll", _context.HolidayDate.ToList()) });
        }

        private bool HolidayDateExists(int id)
        {
            return _context.HolidayDate.Any(e => e.HolidayDateId == id);
        }

        //For Dashboard
        //unused, moved to home controller
        public JsonResult GetEvents()
        {
            var events = _context.HolidayDate.Select(e => new
            {
                id = e.HolidayDateId,
                title = e.Name,
                description = e.Type,
                start = e.StartDate.ToString("MM/dd/yyyy HH:mm:ss"), //please dont change format
                end = e.EndDate.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("MM/dd/yyyy HH:mm:ss")
            }).ToList();
            return new JsonResult(events);
        }

        public JsonResult GetEventsInRange(DateTime start, DateTime end)
        {
            //var events = from e in _context.HolidayDate.Where(e => start <= e.StartDate && end >= e.EndDate) select e;
            TimeSpan t = end - start;
            double totalDays = t.TotalDays+1; //jumlah pengajuan cuti
            double totalHolidays = 0; //jumlah libur pada range tanggal tsb
            IDictionary<DateTime, bool> holidayDateDict = new Dictionary<DateTime, bool>();
            List<HolidayDate> isHoliday = (from e in _context.HolidayDate.Where(e => e.StartDate >= start && e.EndDate <= end) select e).ToList();
            foreach(HolidayDate hd in isHoliday)
            {
                TimeSpan ts = hd.EndDate - hd.StartDate;
                double total = ts.TotalDays; // ex : 2
                for(int i=0;i<=total;i++)
                {
                    if(!holidayDateDict.ContainsKey(hd.StartDate)){
                        holidayDateDict.Add(hd.StartDate, true);
                    }
                    hd.StartDate = hd.StartDate.AddDays(1);
                }
            }
            //count weekend and check based on dictionary
            var userId = this.User.FindFirst(ClaimTypes.NameIdentifier);
            var applicationUser = _context.AppUsers.Find(userId.Value);
            for (int i=0; i<totalDays;i++)
            {
                if((start.DayOfWeek == DayOfWeek.Saturday || start.DayOfWeek == DayOfWeek.Sunday) && applicationUser.EmployeeType == "Daily")
                {
                    if (!holidayDateDict.ContainsKey(start))
                    {
                        holidayDateDict.Add(start, true);
                    }
                }
                start = start.AddDays(1);
            }
            return new JsonResult(totalDays - holidayDateDict.Count);
        }
    }
}
