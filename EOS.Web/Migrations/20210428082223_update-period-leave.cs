﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class updateperiodleave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "PaidLeavePeriod",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.CreateIndex(
                name: "IX_PaidLeavePeriod_UserId",
                table: "PaidLeavePeriod",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaidLeavePeriod_AspNetUsers_UserId",
                table: "PaidLeavePeriod",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaidLeavePeriod_AspNetUsers_UserId",
                table: "PaidLeavePeriod");

            migrationBuilder.DropIndex(
                name: "IX_PaidLeavePeriod_UserId",
                table: "PaidLeavePeriod");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "PaidLeavePeriod",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
