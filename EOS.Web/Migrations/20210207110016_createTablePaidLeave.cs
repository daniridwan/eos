﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class createTablePaidLeave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Type",
                columns: table => new
                {
                    SectionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PaidLeaveName = table.Column<string>(nullable: false),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Type", x => x.SectionId);
                });

            migrationBuilder.CreateTable(
                name: "PaidLeave",
                columns: table => new
                {
                    PaidLeaveId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    TypeSectionId = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: false),
                    requestorId = table.Column<string>(nullable: true),
                    RequestDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaidLeave", x => x.PaidLeaveId);
                    table.ForeignKey(
                        name: "FK_PaidLeave_Type_TypeSectionId",
                        column: x => x.TypeSectionId,
                        principalTable: "Type",
                        principalColumn: "SectionId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaidLeave_AspNetUsers_requestorId",
                        column: x => x.requestorId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PaidLeave_TypeSectionId",
                table: "PaidLeave",
                column: "TypeSectionId");

            migrationBuilder.CreateIndex(
                name: "IX_PaidLeave_requestorId",
                table: "PaidLeave",
                column: "requestorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaidLeave");

            migrationBuilder.DropTable(
                name: "Type");
        }
    }
}
