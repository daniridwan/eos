﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class AddColumnApproverPaidLeave : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FirstApprover",
                table: "PaidLeave",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstApproverResponse",
                table: "PaidLeave",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "FirstApproverTimestamp",
                table: "PaidLeave",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SecondApprover",
                table: "PaidLeave",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SecondApproverResponse",
                table: "PaidLeave",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "SecondApproverTimestamp",
                table: "PaidLeave",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstApprover",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "FirstApproverResponse",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "FirstApproverTimestamp",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "SecondApprover",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "SecondApproverResponse",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "SecondApproverTimestamp",
                table: "PaidLeave");
        }
    }
}
