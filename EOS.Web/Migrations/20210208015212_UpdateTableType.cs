﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class UpdateTableType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaidLeave_Type_TypeSectionId",
                table: "PaidLeave");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Type",
                table: "Type");

            migrationBuilder.DropIndex(
                name: "IX_PaidLeave_TypeSectionId",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "SectionId",
                table: "Type");

            migrationBuilder.DropColumn(
                name: "TypeSectionId",
                table: "PaidLeave");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "Type",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "TypeId",
                table: "PaidLeave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Value",
                table: "PaidLeave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Type",
                table: "Type",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_PaidLeave_TypeId",
                table: "PaidLeave",
                column: "TypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaidLeave_Type_TypeId",
                table: "PaidLeave",
                column: "TypeId",
                principalTable: "Type",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaidLeave_Type_TypeId",
                table: "PaidLeave");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Type",
                table: "Type");

            migrationBuilder.DropIndex(
                name: "IX_PaidLeave_TypeId",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Type");

            migrationBuilder.DropColumn(
                name: "TypeId",
                table: "PaidLeave");

            migrationBuilder.DropColumn(
                name: "Value",
                table: "PaidLeave");

            migrationBuilder.AddColumn<int>(
                name: "SectionId",
                table: "Type",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddColumn<int>(
                name: "TypeSectionId",
                table: "PaidLeave",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Type",
                table: "Type",
                column: "SectionId");

            migrationBuilder.CreateIndex(
                name: "IX_PaidLeave_TypeSectionId",
                table: "PaidLeave",
                column: "TypeSectionId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaidLeave_Type_TypeSectionId",
                table: "PaidLeave",
                column: "TypeSectionId",
                principalTable: "Type",
                principalColumn: "SectionId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
