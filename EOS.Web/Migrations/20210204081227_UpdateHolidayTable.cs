﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class UpdateHolidayTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "HolidayDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDate",
                table: "HolidayDate",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDate",
                table: "HolidayDate",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDate",
                table: "HolidayDate");

            migrationBuilder.DropColumn(
                name: "StartDate",
                table: "HolidayDate");

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "HolidayDate",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
