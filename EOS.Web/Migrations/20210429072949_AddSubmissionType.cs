﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class AddSubmissionType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SubmissionType",
                table: "PaidLeave",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubmissionType",
                table: "PaidLeave");
        }
    }
}
