﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class UpdateDepartmentHead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DepartmentHead",
                table: "Department",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartmentHead",
                table: "Department");
        }
    }
}
