﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EOS.Web.Migrations
{
    public partial class createTablePaidLeaverev1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaidLeave_AspNetUsers_requestorId",
                table: "PaidLeave");

            migrationBuilder.RenameColumn(
                name: "requestorId",
                table: "PaidLeave",
                newName: "RequestorId");

            migrationBuilder.RenameIndex(
                name: "IX_PaidLeave_requestorId",
                table: "PaidLeave",
                newName: "IX_PaidLeave_RequestorId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaidLeave_AspNetUsers_RequestorId",
                table: "PaidLeave",
                column: "RequestorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaidLeave_AspNetUsers_RequestorId",
                table: "PaidLeave");

            migrationBuilder.RenameColumn(
                name: "RequestorId",
                table: "PaidLeave",
                newName: "requestorId");

            migrationBuilder.RenameIndex(
                name: "IX_PaidLeave_RequestorId",
                table: "PaidLeave",
                newName: "IX_PaidLeave_requestorId");

            migrationBuilder.AddForeignKey(
                name: "FK_PaidLeave_AspNetUsers_requestorId",
                table: "PaidLeave",
                column: "requestorId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
