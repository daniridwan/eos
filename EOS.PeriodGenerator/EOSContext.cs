﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;
using EOS.PeriodGenerator.Models;

namespace EOS.PeriodGenerator
{
    public class EOSContext : DbContext
    {
        public DbSet<AspNetUsers> AspNetUsers { get; set; }
        public DbSet<PaidLeavePeriod> PaidLeavePeriod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false).Build();
            if(!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=EOS;Trusted_Connection=True;");
                optionsBuilder.UseSqlServer(config.GetConnectionString("EOSContextConnection"));
            }
            
        }
    }
}
