﻿using System;
using EOS.PeriodGenerator.Models;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace EOS.PeriodGenerator
{
    class Program
    {
        static void Main(string[] args) //schedule every 
        {
            var context = new EOSContext();
            var employeeList = context.AspNetUsers.Where(s => s.EndJoinDate == DateTime.MinValue).ToList(); //active employee
            foreach (var i in employeeList)
            {
                PaidLeavePeriod p = new PaidLeavePeriod();
                p.UserId = i.Id;
                p.StartDate = new DateTime(DateTime.Now.Year, 01, 01);
                p.EndDate = p.StartDate.AddMonths(18).AddDays(-1); // 18 months duration
                p.Value = 12;
                p.Status = 1;
                p.PeriodName = string.Format("Periode Cuti {0}", DateTime.Now.Year.ToString());
                context.PaidLeavePeriod.Add(p);
                context.SaveChanges();
                Console.WriteLine(string.Format("Paid Leave Period {0} for user {1} created", DateTime.Now.Year.ToString(), i.UserName));
            }
        }
    }
}
