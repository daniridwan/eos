﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EOS.PeriodGenerator.Models
{
    public class AspNetUsers
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime StartJoinDate { get; set; }
        public DateTime EndJoinDate { get; set; }
    }
}
