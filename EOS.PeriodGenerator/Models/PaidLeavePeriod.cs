﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace EOS.PeriodGenerator.Models
{
    public class PaidLeavePeriod
    {
        [Key]
        public int Id { get; set; }
        public string PeriodName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Value { get; set; }
        public int Status { get; set; }
        public AspNetUsers User { get; set; }
        public string UserId { get; set; }
    }
}
